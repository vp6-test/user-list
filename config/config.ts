import { defineConfig } from 'umi';

export default defineConfig({
  antd: {},
  define: {},
  devServer: {
    port: 3000,
    http2: true,
  },
  dynamicImport: {},
  hash: true,
  manifest: {
    basePath: '/',
  },
  mock: {},
  request: false,
  routes: [
    {
      path: '/',
      component: '@/pages/user/list',
      name: 'Users',
    }
  ],
  targets: {
    ie: 11,
  },
  title: 'VP6 Test',
});
