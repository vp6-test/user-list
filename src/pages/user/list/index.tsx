import React from 'react';
import { connect, Dispatch, Loading } from 'umi';
import { UserState } from '@/models/user';
import './index.less';


interface IProps {
  user: UserState;
  dispatch: Dispatch;
  loading: boolean;
}

class UserList extends React.Component<IProps> {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'user/getUsers',
    });
  }

  render() {
    const { loading, user } = this.props;

    console.log(loading, user.list);

    return null;
  }
}

interface IConnect {
  user: UserState;
  loading: Loading;
}

export default connect(({ user, loading }: IConnect) => ({
  user,
  loading: loading.models.user,
}))(UserList);
