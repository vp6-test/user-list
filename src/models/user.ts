import { Effect, Reducer } from 'umi';
import { getUsers } from '@/services/user';


interface User {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  gender: string;
}

export interface UserState {
  list: User[];
}

interface IUserModel {
  namespace: string;
  state: UserState;
  effects: {
    getUsers: Effect;
  };
  reducers: {
    update: Reducer<UserState>;
  };
}

const UserModel: IUserModel = {
  namespace: 'user',

  state: {
    list: [],
  },

  effects: {
    *getUsers(_, { call, put }) {
      const response = yield call(getUsers);

      yield put({
        type: 'update',
        payload: {
          list: response.map((user: any) => ({
            ...user,
            key: user.id,
          })),
        },
      });
    },
  },

  reducers: {
    update(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default UserModel;
