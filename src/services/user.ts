import request from '@/utils/request';


export async function getUsers(): Promise<any> {
  return request('/api/user/list');
}
